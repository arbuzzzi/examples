/* eslint-disable */
// noinspection ES6UnusedImports,JSFileReferences,JSLastCommaInObjectLiteral
// noinspection JSUnusedLocalSymbols

const fs = require('fs');
const path = require('path');

/*
 * Arguments
 */
if (process.argv.length !== 3 && process.argv.length !== 4) {
  console.log('Не верное количество аргументов. Попробуйте `yarn cc <ComponentName>`.');
  process.exit(1);
}

const arg = process.argv[2];
const dir = process.argv[3];
const filename = arg.toLowerCase().charAt(0).toUpperCase() + arg.slice(1);
const component = arg.charAt(0).toUpperCase() + arg.slice(1);

/*
 * Component dir
 */
const baseDir = dir ? `./src/${dir}` : './src/Components';
const directory = `${baseDir}/${filename}`;

if (fs.existsSync(directory)) {
  console.log(`Компонент ${component} не был создан, потому что директория ${directory} уже существует.`);
  process.exit(1);
}

fs.mkdirSync(directory);

/*
 * Component file
 */
const componentFileName = path.join(directory, 'index.jsx');

/*
 * Component file template
 */
const componentFileContents = `import PropTypes from 'prop-types';
import clsx from 'clsx';
import defaultPropsTypes from '@helpers/defaultPropsTypes';
import s from './${component}.module.scss';

function ${component}({ children, className, style }) {
  return (
    <div className={clsx(className, s.${component})} style={style}>
      {children}
    </div>
  );
}

${component}.propTypes = {
  ...defaultPropsTypes,
};

${component}.defaultProps = {

};

export default ${component};
`;

fs.writeFileSync(componentFileName, componentFileContents);

/*
 * Style file
 */
const typesFileName = path.join(directory, `${component}.module.scss`);
const typesFileContents = `.${component} {

}
`;

fs.writeFileSync(typesFileName, typesFileContents);

const addData = `export ${component} from './${component}';
`;
fs.appendFile(`${baseDir}/index.jsx`, addData, (err) => {
  if (err) throw err;
  console.log('Saved!');
});

console.log(`Компонент ${component} успешно создан в папке ${directory}`);
