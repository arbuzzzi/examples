/* eslint-disable */
// noinspection ES6UnusedImports,JSUnusedLocalSymbols
// noinspection JSUnusedLocalSymbols

const fs = require('fs');
const path = require('path');

/*
 * Arguments
 */
if (process.argv.length !== 3 && process.argv.length !== 4) {
  console.log('Не верное количество аргументов. Попробуйте `yarn cs sliceName`.');
  process.exit(1);
}

const arg = process.argv[2];
const dir = process.argv[3];
const fileName = arg.toLowerCase().charAt(0).toUpperCase() + arg.slice(1);
const sliceName = arg.charAt(0).toLowerCase() + arg.slice(1);

/*
 * Component dir
 */
const baseDir = './src/redux';
const directory = `${baseDir}/slices/${fileName}`;

if (fs.existsSync(directory)) {
  console.log(`Компонент ${sliceName} не был создан, потому что директория ${directory} уже существует.`);
  process.exit(1);
}

fs.mkdirSync(directory);

/*
 * component file
 */
const componentFileName = path.join(directory, 'index.js');
const componentFileContents = `import { createSlice } from '@reduxjs/toolkit';
import { HTTP_STATUS } from '@constants';
import { fetch${fileName} } from './thunk';

const ${sliceName}Slice = createSlice({
  name: '${sliceName}',
  initialState: {
    status: null,
    payload: null,
  },
  reducers: {},
  extraReducers: {
    [fetch${fileName}.pending](state) {
      state.status = HTTP_STATUS.PENDING;
    },
    [fetch${fileName}.fulfilled](state, { payload }) {
      state.status = HTTP_STATUS.SUCCESS;
      state.payload = payload;
    },
    [fetch${fileName}.rejected](state) {
      state.status = HTTP_STATUS.FAILED;
    },
  },
});

export default ${sliceName}Slice.reducer;
`;

fs.writeFileSync(componentFileName, componentFileContents);

/*
 * thunk file
 */
const typesFileName = path.join(directory, 'thunk.js');
const typesFileContents = `import { createAsyncThunk } from '@reduxjs/toolkit';

export const fetch${fileName} = createAsyncThunk(
  '${sliceName}/getAll',
  async () => ({}),
);
`;

fs.writeFileSync(typesFileName, typesFileContents);

const addData = `export ${fileName}Reducer from './${fileName}';
`;
fs.appendFile(`${baseDir}/slices/index.js`, addData, (err) => {
  if (err) throw err;
  console.log('Saved!');
});

console.log(`Компонент ${sliceName} успешно создан в папке ${directory}`);
