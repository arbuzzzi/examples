import better from '@Assets/images/better.svg';
import faster from '@Assets/images/faster.svg';
import stronger from '@Assets/images/stronger.svg';

import betterEn from '@Assets/images/better_en.svg';
import fasterEn from '@Assets/images/faster_en.svg';
import strongerEn from '@Assets/images/stronger_en.svg';

import advantage1 from '@Assets/images/advantage1.webp';
import advantage2 from '@Assets/images/advantage2.webp';
import advantage3 from '@Assets/images/advantage3.webp';
import advantage4 from '@Assets/images/advantage4.webp';
import advantage5 from '@Assets/images/advantage5.webp';
import advantage6 from '@Assets/images/advantage6.webp';
import advantage7 from '@Assets/images/advantage7.webp';

export const PROMO_ITEMS = {
  ru: [
    {
      id: 'better',
      image: better,
    },
    {
      id: 'faster',
      image: faster,
    },
    {
      id: 'stronger',
      image: stronger,
    },
  ],

  en: [
    {
      id: 'better',
      image: betterEn,
    },
    {
      id: 'faster',
      image: fasterEn,
    },
    {
      id: 'stronger',
      image: strongerEn,
    },
  ],
};

export const ADVANTAGES = [
  {
    id: 'AdvantageItem1',
    image: advantage1,
  },
  {
    id: 'AdvantageItem2',
    image: advantage2,
  },
  {
    id: 'AdvantageItem3',
    image: advantage3,
  },
  {
    id: 'AdvantageItem4',
    image: advantage4,
  },
  {
    id: 'AdvantageItem5',
    image: advantage5,
  },
  {
    id: 'AdvantageItem6',
    image: advantage6,
  },
  {
    id: 'AdvantageItem7',
    image: advantage7,
  },
];
