import { Header } from '@Components';
import { Button, Divider, Typography } from '@Components/Ui';
import getState from '@helpers/getState';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import {
  Tab, TabList, TabPanel, Tabs,
} from 'react-tabs';
import useQuery from '../../hooks/useQuery';
import { ADVANTAGES, PROMO_ITEMS } from '../../mocks/promoItems';
import s from './Home.module.scss';

export default function Home() {
  const query = useQuery();

  const { language, locales } = useSelector(getState('locales'));

  const [tabIndex, setTabIndex] = useState(0);
  useEffect(() => {
    const index = parseInt(query?.get('tab'), 10);
    setTabIndex(index <= 1 ? index : 0);
  }, [query]);

  if (!locales) return null;

  return (
    <div className={s.Home}>
      <div className="container">
        <Header />
        <Tabs selectedIndex={tabIndex} onSelect={(index) => setTabIndex(index)}>
          <TabList>
            <Tab>
              <Button variant={tabIndex === 0 ? 'dark' : 'light'}>{locales?.betterButtonName}</Button>
            </Tab>
            <Tab>
              <Button variant={tabIndex === 1 ? 'dark' : 'light'}>{locales?.pepsiCoButtonName}</Button>
            </Tab>
          </TabList>
          <TabPanel>
            <div className="tabContent">
              <Typography>{locales?.betterDescription}</Typography>
              {PROMO_ITEMS?.[language]?.map((item) => (
                <div key={item.id} className={s.tabContentItem}>
                  <div className={s.left}>
                    <img src={item.image} alt="" />
                  </div>
                  <div className={s.right}>
                    <Typography>{locales?.[item.id]}</Typography>
                  </div>
                </div>
              ))}
            </div>
          </TabPanel>
          <TabPanel>
            <div className={`tabContent ${s.tabContentAdvantages}`}>
              <Typography>{locales?.pepsiCoDescription}</Typography>
              {ADVANTAGES?.map((item) => (
                <div key={item.id} className={s.tabContentAdvantageItem}>
                  <img src={item.image} alt="" />
                </div>
              ))}
            </div>
          </TabPanel>
        </Tabs>
      </div>
      <Divider />
    </div>
  );
}
