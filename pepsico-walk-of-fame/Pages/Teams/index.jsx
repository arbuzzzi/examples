import { Header } from '@Components';
import { TeamModal } from '@Components/Modals';
import { Button, Divider, Typography } from '@Components/Ui';
import getState from '@helpers/getState';
import { setTeamsModalData } from '@slices/Teams';
import { fetchTeams } from '@slices/Teams/thunk';
import { fetchUsers } from '@slices/Users/thunk';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Tab, TabList, TabPanel, Tabs,
} from 'react-tabs';
import useQuery from '../../hooks/useQuery';
import { ADVANTAGES, PROMO_ITEMS } from '../../mocks/promoItems';
import s from './Teams.module.scss';

export default function Teams() {
  const dispatch = useDispatch();
  const query = useQuery();

  const { payload: TeamsData } = useSelector(getState('teams'));
  const { language, locales } = useSelector(getState('locales'));

  const [tabIndex, setTabIndex] = useState(0);
  const [useModal, setModal] = useState(false);

  useEffect(() => {
    dispatch(fetchTeams());
    dispatch(fetchUsers());
  }, [dispatch]);

  useEffect(() => {
    const index = parseInt(query?.get('tab'), 10);
    setTabIndex(index <= 1 ? index : 0);
  }, [query]);

  const openTeamModal = useCallback((data) => {
    dispatch(setTeamsModalData(data));
    setModal(true);
  }, [dispatch]);

  if (!locales) return null;

  return (
    <div className={s.Teams}>
      <TeamModal isOpen={useModal} onRequestClose={() => setModal(!useModal)} />
      <div className={`container ${s.head}`}>
        <Header />
      </div>
      <Tabs selectedIndex={tabIndex} onSelect={(index) => setTabIndex(index)}>
        <div className="container">
          <TabList>
            <Tab>
              <Button variant={tabIndex === 0 ? 'dark' : 'light'}>{locales?.betterButtonName}</Button>
            </Tab>
            <Tab>
              <Button variant={tabIndex === 1 ? 'dark' : 'light'}>{locales?.pepsiCoButtonName}</Button>
            </Tab>
          </TabList>
        </div>
        <TabPanel>
          <div className="tabContent">
            <div className="container">
              <Typography>{locales?.betterDescription}</Typography>
            </div>
            {PROMO_ITEMS?.[language]?.map((item, i) => (
              <>
                <div key={item.id} className={`container ${s.PromoItem}`}>
                  <div className={s.rowTitle}><img src={item?.image} alt="" /></div>
                  <div className={s.russia}>
                    {i === 0 && <div className={s.teamsTitle}>RUSSIA</div>}
                    {TeamsData?.filter((team) => team.promoId === item.id && team.type === 'RUSSIA').map((team, teamI) => (
                      <>
                        {teamI === 0 && <div className={`${s.teamsTitle} ${s.mobile}`}>RUSSIA</div>}
                        <div key={team.id} className={s.teamItem}>
                          <Button variant="thin" onClick={() => openTeamModal(team)}>{team.name}</Button>
                        </div>
                      </>
                    ))}
                  </div>
                  <div className={s.bucca}>
                    {i === 0 && <div className={s.teamsTitle}>BUCCA</div>}
                    {TeamsData?.filter((team) => team.promoId === item.id && team.type === 'BUCCA').map((team, teamI) => (
                      <>
                        {teamI === 0 && <div className={`${s.teamsTitle} ${s.mobile}`}>BUCCA</div>}
                        <div key={team.id} className={s.teamItem}>
                          <Button variant="thin" onClick={() => openTeamModal(team)}>{team.name}</Button>
                        </div>
                      </>
                    ))}
                  </div>
                </div>
                {i < PROMO_ITEMS?.[language]?.length - 1 && <Divider />}
              </>
            ))}
          </div>
        </TabPanel>
        <TabPanel>
          <div className="tabContent container">
            <Typography>{locales?.pepsiCoDescription}</Typography>
            {ADVANTAGES?.map((item) => (
              <div key={item.id} className={s.Advantages}>
                <div className={s.advantageTitle}><img src={item?.image} alt="" /></div>
                <div className={s.advantageTeams}>
                  {TeamsData?.filter((team) => team.advantagesId === item.id).map((team) => (
                    <div key={team.id} className={s.teamItem}>
                      <Button variant="thin" onClick={() => openTeamModal(team)}>{team.name}</Button>
                    </div>
                  ))}
                </div>
              </div>
            ))}
          </div>
        </TabPanel>
      </Tabs>
      {tabIndex === 1 && <Divider />}
    </div>
  );
}
