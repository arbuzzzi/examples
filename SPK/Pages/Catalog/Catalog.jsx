import { useEffect } from 'react';
import { fetchCatalog } from '@slices/Catalog/thunk';
import { Typography } from '@Components/Ui';
import { getLocales } from '@helpers/getLocales';
import { useSelector, useDispatch } from 'react-redux';
import getState from '@helpers/getState';
import { setRuntimeState } from '@slices/Runtime';
import { defaultSEO } from '@constants';
import Layout from '@Components/Layout';
import { Link, useLocation } from 'react-router-dom';
import { SearchCatalog } from '@Components/SearchBlocks';

import * as images from '@Assets/img/catalog';

import clsx from 'clsx';
import s from './Catalog.module.scss';

function Catalog() {
  const { payload: CategoriesData } = useSelector(getState('categories'));
  const { payload: CatalogData } = useSelector(getState('catalog'));
  const { payload: { text: Lang } } = useSelector(getState('language'));

  const location = useLocation();

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCatalog());
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      setRuntimeState({
        name: 'seo',
        value: CatalogData?.seo || defaultSEO,
      }),
    );
  }, [CatalogData, dispatch]);

  return (
    <Layout>
      <div className={clsx(s.Catalog, 'page')}>
        <div className={s.bg} />
        <div className="content">
          <div className={s.header}>
            <Typography variant="title">{getLocales(CatalogData, Lang)?.name}</Typography>
            <SearchCatalog className={s.search} />
          </div>
          <Typography variant="text" className={s.description}>{getLocales(CatalogData, Lang)?.description}</Typography>
          <SearchCatalog className={s.searchMobile} wrapClassName={s.searchMobileWrap} />
          <div className={s.items}>
            {CategoriesData?.filter((item) => !item?.parentId)?.map((item) => (
              <div key={item?.id} className={s.item}>
                <Typography className={s.title} variant="subtitle-light">
                  <Link to={{
                    pathname: `${location.pathname}${item?.link}`,
                    state: item,
                  }}
                  >
                    {getLocales(item, Lang)?.name}
                  </Link>
                </Typography>
                <div className={s.box}>
                  <div className={s.imgBox}>
                    <Link to={{
                      pathname: `${location.pathname}${item?.link}`,
                      state: item,
                    }}
                    >
                      <img src={images?.[item?.imageName]} alt="" />
                    </Link>
                  </div>
                  <div className={s.links}>
                    {CategoriesData?.filter((el) => item?.id === el?.parentId)?.map((subItem) => (
                      <div key={subItem?.id} className={clsx(s.link)}>
                        <Link
                          to={{
                            pathname: `${location.pathname}${item?.link}${subItem?.link}`,
                            state: subItem,
                          }}
                          className="link"
                        >
                          { subItem?.name }
                        </Link>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default Catalog;
