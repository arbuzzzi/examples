import loadable from '@loadable/component';

const Catalog = loadable(() => import('./Catalog'));

const route = {
  key: 2,
  path: '/catalog',
  component: Catalog,
};

export default route;
