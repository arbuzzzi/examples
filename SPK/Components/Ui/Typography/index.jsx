import PropTypes from 'prop-types';
import defaultPropsTypes from '@helpers/defaultPropsTypes';
import parse from 'html-react-parser';

import clsx from 'clsx';
import s from './Typography.module.scss';

function Typography({
  children,
  onClick,
  className,
  component,
  variant,
  mb,
  mt,
  style,
  lightColor,
}) {
  const Component = component;
  const child = typeof children === 'string' ? parse(children) : children;

  const getStyles = () => {
    const styles = {};

    if (mb) styles.marginBottom = mb;
    if (mt) styles.marginTop = mt;

    return styles;
  };

  const isButton = Component === 'button' || Component === 'input';

  return (
    <Component
      className={clsx(className, s.Typography, s[variant], lightColor && s.lightColor, {
        [s.pointer]: isButton,
      })}
      style={{ ...getStyles(), ...style }}
      onClick={onClick}
      onKeyDown={onClick}
      role={isButton && 'button'}
      tabIndex={isButton && 0}
    >
      { child }
    </Component>
  );
}

Typography.propTypes = {
  ...defaultPropsTypes,
  onClick: PropTypes.func,
  variant: PropTypes.oneOf([
    'text', 'title', 'description',
    'mainTitle', 'mainSubtitle', 'mainText',
    'mainTextSmall', 'miniText', 'smallText',
    'bigCount', 'subtitle', 'slogan',
    'sloganMini', 'namingText', 'itemTitle',
    'itemDesc', 'quote', 'nav',
    'serviceTitle', 'subtitle-light',
    'cardTitle', 'errorMessage', 'linkByTitle',
    'formTitle', 'errorText', 'labelText',
  ]),
  component: PropTypes.node,
  lightColor: PropTypes.bool,
  mb: PropTypes.string,
  mt: PropTypes.string,
};

Typography.defaultProps = {
  onClick: () => null,
  variant: 'text',
  component: 'div',
  mb: null,
  mt: null,
  lightColor: false,
};

export default Typography;
