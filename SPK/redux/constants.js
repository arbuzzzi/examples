export const HTTP_STATUS = Object.freeze({
  PENDING: 'PENDING',
  SUCCESS: 'SUCCESS',
  FAILED: 'FAILED',
});

export const defaultSEO = {
  title: 'СПК | Сибирская промышленная компания',
  description: 'Современное оборудование для горной промышленности',
  locales: {
    ru: {
      title: 'СПК | Сибирская промышленная компания',
      description: 'Современное оборудование для горной промышленности',
    },
    en: {
      title: 'SPK | Siberian Industrial Company',
      description: 'Modern mining equipment',
    },
  },
};
