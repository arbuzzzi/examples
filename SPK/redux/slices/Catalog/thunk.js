import { createAsyncThunk } from '@reduxjs/toolkit';
import { CATALOG } from '@mocks/catalog';

export const fetchCatalog = createAsyncThunk(
  'catalog/getAll',
  async () => (CATALOG),
);
