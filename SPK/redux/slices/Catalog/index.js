import { createSlice } from '@reduxjs/toolkit';
import { HTTP_STATUS } from '@constants';
import { fetchCatalog } from './thunk';

const catalogSlice = createSlice({
  name: 'catalog',
  initialState: {
    status: null,
    payload: null,
  },
  reducers: {},
  extraReducers: {
    [fetchCatalog.pending](state) {
      state.status = HTTP_STATUS.PENDING;
    },
    [fetchCatalog.fulfilled](state, { payload }) {
      state.status = HTTP_STATUS.SUCCESS;
      state.payload = payload;
    },
    [fetchCatalog.rejected](state) {
      state.status = HTTP_STATUS.FAILED;
    },
  },
});

export default catalogSlice.reducer;
