import {
  CatalogReducer,
} from './slices';

const rootReducer = {
  catalog: CatalogReducer,
};

export default rootReducer;
